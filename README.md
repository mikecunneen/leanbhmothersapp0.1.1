# README #

To run this project you need to have the Ionic Framework (Cordova) installed. You can do this here: http://ionicframework.com/getting-started/ . It will ask you to install NodeJS / Node Package Manager (NPM) also. 

Then, the easiest way to the run the project in the web browser (locally) is to cd into the project directory and then type: ionic serve (this will run the web app within your browser via local NodeJS web server.

To run the app on a phone (e.g. Android) do this (and ensure that your phone is plugged into your computer):


```
#!PHP

cd into the project directory type these 3 commands, one after another:
ionic platform add android
ionic build android
ionic run android
```


The app will now install and run on your phone.

For ios do this (you need a Mac!):

```
#!php

ionic platform add ios
ionic build ios

```

Go to Project Directory/platforms/ios and open the .xcode project file. With your iPhone plugged in, run the app and it will install and run on your iPhone.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact